"-----------------------------------------------------------------------------------GENERAL-VIM-SETTINGS----------------------------------------------------------------------------------------------------"

"nvim spacing settings
  filetype plugin indent on
  "On pressing tab, insert 2 spaces
  set expandtab
  "Show existing tab with 2 spaces width
  set tabstop=2
  set softtabstop=2
  "When indenting with '>', use 2 spaces width
  set shiftwidth=2
"Set Text editor line numbers  
  set number
"Auto Reload files when changed on Vim
  set autoread
"Auto Reload when cursor is not moving
  au CursorHold,CursorHoldI * :checktime
  au FocusGained,bufenter * :checktime
  set syntax 
  set smarttab
  set cindent
"Reduce Vim Update time
	set updatetime=10
"Set clipoard to the main one
  set clipboard=unnamedplus

"---------------------------------------------------------------------------------END-OF-GENERAL-VIM-SETTINGS-----------------------------------------------------------------------------------------------"
"--------------------------------------------------------------------------------------VIM-PLUG-SETTINGS----------------------------------------------------------------------------------------------------"
  
  call plug#begin('~/.local/share/nvim/plugged')
  "Plugins List for Vim-plug
    "Auto completion Plugin
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    "Left Side file Tree Plugin
    Plug 'scrooloose/nerdtree'
    
    "NerdTree - Git Icons/colors
    Plug 'MiChatz/nerdtree-git-plugin'
    "Git indentation on the left side 
    Plug 'airblade/vim-gitgutter'
    
    "Merge tool
    Plug 'samoshkin/vim-mergetool'

    "Multi cursor plugin
    Plug 'terryma/vim-multiple-cursors'

    "Status bar
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'

    "Git Blame tool
    Plug 'APZelos/blamer.nvim'
    "Fuzzy Finder
    "Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
    "Fuzzy Finder DevIcons
    Plug 'coreyja/fzf.devicon.vim'
    "Code Block commenter
    Plug 'scrooloose/nerdcommenter'
    "Javascript auto indentation and auto completion
    "Plug 'prettier/vim-prettier', { 'do': 'yarn install' }
    
    "Markdown Indentation
    Plug 'godlygeek/tabular'
    Plug 'plasticboy/vim-markdown'
    
    "Markdown Instand server
    Plug 'suan/vim-instant-markdown', {'for': 'markdown'}    
    
    "Grammar Check
    Plug 'rhysd/vim-grammarous'
    
    "Lines indentation next to functions
    Plug 'Yggdroot/indentLine'
    "Tmux Navigator inside Vim 
    Plug 'christoomey/vim-tmux-navigator'
    "VSCode theme
    Plug 'tomasiser/vim-code-dark'
    "JSX highlight
    Plug 'mxw/vim-jsx'
    "JS highlight
    Plug 'pangloss/vim-javascript'
    "TS highlight  
    Plug 'HerringtonDarkholme/yats.vim'
    Plug 'mhartington/nvim-typescript', {'do': './install.sh'}
    "Ruby Plug
    Plug 'vim-ruby/vim-ruby'
    "Rails Plug
    Plug 'tpope/vim-rails'

    "Dev Icons colors
    "Plug 'vwxyutarooo/nerdtree-devicons-syntax'
    "Dev Icons for NerdTree (It needs specific font installed)
    Plug 'ryanoasis/vim-devicons'
  call plug#end()

"------------------------------------------------------------------------------------END-VIM-PLUG-SETTINGS-------------------------------------------------------------------------------------------------"
"------------------------------------------------------------------------------------COLOR-THEME-SETTINGS--------------------------------------------------------------------------------------------------"
  set termguicolors
  colorscheme codedark
  let g:javascript_plugin_flow = 1
  let g:javascript_plugin_jsdoc = 1
 "set renderoptions=type:directx,gamma:1.5,contrast:0.5,geom:1,renmode:5,taamode:1,level:0.5 
  "let cdLightBlue = {'gui': '#9CDCFE', 'cterm': '06', 'cterm256': '117'}
"hi Normal guibg=NONE ctermbg=NONE
"Highlighting function (inspiration from https://github.com/chriskempson/base16-vim)
"if &t_Co >= 256
    "let g:codedark_term256=1
"elseif !exists("g:codedark_term256")
    "let g:codedark_term256=0
"endif
"fun! Hi(group, fg, bg, attr, sp)
  "if !empty(a:fg)
    "exec "hi " . a:group . " guifg=" . a:fg.gui . " ctermfg=" . (g:codedark_term256 ? a:fg.cterm256 : a:fg.cterm)
  "endif
  "if !empty(a:bg)
    "exec "hi " . a:group . " guibg=" . a:bg.gui . " ctermbg=" . (g:codedark_term256 ? a:bg.cterm256 : a:bg.cterm)
  "endif
  "if a:attr != ""
    "exec "hi " . a:group . " gui=" . a:attr . " cterm=" . a:attr
  "endif
  "if !empty(a:sp)
    "exec "hi " . a:group . " guisp=" . a:sp.gui
  "endif
"endfun

  "call Hi('xmlTag', cdLightBlue, {}, 'none', {})
  "call Hi('xmlTagName', cdLightBlue, {}, 'none', {})
  "call Hi('xmlEndTag', cdLightBlue, {}, 'none', {})

  "Line Intenetion character
  let g:indentLine_char = '│'
"---------------------------------------------------------------------------------END-COLOR-THEME-SETTINGS--------------------------------------------------------------------------------------------------"

"--------------------------------------------------------------------------------------AIRLINE-SETTINGS-----------------------------------------------------------------------------------------------------"
set laststatus=0  
let g:airline_statusline_ontop=1
let g:airline#extensions#tabline#enabled = 1
"----------------------------------------------------------------------------------END-OF-AIRLINE-SETTINGS--------------------------------------------------------------------------------------------------"

"------------------------------------------------------------------------------------MERGE-TOOL-SETTINGS----------------------------------------------------------------------------------------------------"
  let g:mergetool_layout = 'mr,b'
  let g:mergetool_prefer_revision = 'local'
  
    set syntax=on
    set nodiff
  let g:mergetool_use_tab = 0
  resize 15

  highlight DiffAdd guibg=#25403b
  highlight DiffDelete guifg=#4a4a4c guibg=#252526
  highlight DiffChange guibg=#25394b
  highlight DiffText guibg=#3a5975
"--------------------------------------------------------------------------------END-OF-MERGE-TOOL-SETTINGS------------------------------------------------------------------------------------------------"
"---------------------------------------------------------------------------------------KEY-BINDINGS--------------------------------------------------------------------------------------------------------"
  "Tmux Vim navigator
    nnoremap <silent> <S-Left> :TmuxNavigateLeft<cr>
    nnoremap <silent> <S-Down> :TmuxNavigateDown<cr>
    nnoremap <silent> <S-Up> :TmuxNavigateUp<cr>
    nnoremap <silent> <S-Right> :TmuxNavigateRight<cr>
    nnoremap <silent> <S-Esc> :TmuxNavigatePrevious<cr>
  "Ctrl + B for toggle NerdTree  
    nmap <C-b> :NERDTreeToggle<CR>
    map  .. :tabn<CR>
    map  ,, :tabp<CR>
    map  // :tabnew<CR>
    let NERDTreeMapOpenSplit = 'h'
    let NERDTreeMapOpenVSplit = 'v'

  "Ctrl + / for comments 
    vmap <C-_> <plug>NERDCommenterToggle
    nmap <C-_> <plug>NERDCommenterToggle 
  "Ctrl + p for fuzzy finder Plug
    nmap <C-p> :GFilesWithDevicons <CR>
    nmap <C-f> :Ag <CR>
  "Ctrl + S for save current file
    noremap <silent> <C-s> :update<CR>
    vnoremap <silent> <C-s> <C-C>:update<CR>
    inoremap <silent> <C-s> <C-O>:update<CR>
  "Git Gutter
    "gp for gitGutter diffs
    nmap gp :call CloseFloatWinBefore()<CR>
    "gs for accept specific gitGutter Hunk
    nmap gp :call GitGutterPreviewHunk<CR>
    nmap gs :GitGutterStageHunk<CR>
    "gs for undo specific gitGutter Hunk
	  nmap gu :GitGutterUndoHunk<CR>
  " Execute the current buffer with ruby 
    noremap <Leader>rx :!ruby % <CR>

"-----------------------------------------------------------------------------------END-OF-KEY-BINDINGS-----------------------------------------------------------------------------------------------------"
"------------------------------------------------------------------------------------NERDTREE-SETTINGS------------------------------------------------------------------------------------------------------"
    "open NERDTree automatically being ON a dir
      autocmd StdinReadPre * let s:std_in=1
      autocmd VimEnter * NERDTree
    "open NERDTree automatically with a dir (ex: vmin <dir>)
      autocmd StdinReadPre * let s:std_in=1
      autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
      autocmd BufWinEnter * NERDTreeMirror " open in every new tab
      "autocmd BufWinEnter * wincmd p " when opened in new tab, switch to FILE buffer (default is the NERDTree buffer)“
    "remove help line
      let NERDTreeMinimalUI=1
    "remove arrows
      let g:NERDTreeDirArrowExpandable = ''
      let g:NERDTreeDirArrowCollapsible = ''  
    "ignore node modules
     let g:NERDTreeIgnore = ['^node_modules$']
    "NerdTree style
      let g:NERDTreeGitStatusWithFlags = 0
      let g:WebDevIconsUnicodeDecorateFolderNodes = 1
      let g:NERDTreeGitStatusNodeColorization = 1
    "NerdTree Git Colors
      let g:NERDTreeColorMapCustom = {
        \ "Modified"  : "#e1c46b",  
        \ "Dirty"     : "#6c6cc4",
        \ "Staged"    : "#f88070",  
        \ "Untracked" : "#73CE90",  
        \ "Clean"     : "#87939A",   
        \ "Ignored"   : "#808080"   
      \ }
    
  "Remove the [] around the Devicons
    augroup nerdtreeconcealbrackets
          autocmd!
          autocmd FileType nerdtree syntax match hideBracketsInNerdTree "\]" contained conceal containedin=ALL cchar= 
          autocmd FileType nerdtree syntax match hideBracketsInNerdTree "\[" contained conceal containedin=ALL
          autocmd FileType nerdtree setlocal conceallevel=2
          autocmd FileType nerdtree setlocal concealcursor=nvic
    augroup END

  "Remove / after folders
    augroup nerdtreehidetirslashes
      autocmd!
      autocmd FileType nerdtree setlocal conceallevel=3 | syntax match NERDTreeDirSlash #/$# containedin=NERDTreeDir conceal contained
    augroup end

  "remove path line
      augroup nerdtreehidecwd
        autocmd!
        autocmd FileType nerdtree syntax match NERDTreeHideCWD #^[</].*$# conceal
      augroup end
  
    "sync open file with NERDTree
    "Check if NERDTree is open or active
      function! IsNERDTreeOpen()        
        return exists("t:NERDTreeBufName") && (bufwinnr(t:NERDTreeBufName) != -1)
      endfunction
    "Can colse Vim with only NERDTree Open
      autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
    "Call NERDTreeFind iff NERDTree is active, current window contains a modifiable
    "file, and we're not in vimdiff
      function! SyncTree()
        if &modifiable && IsNERDTreeOpen() && strlen(expand('%')) > 0 && !&diff
            NERDTreeFind
          wincmd p
        endif
      endfunction

  "Highlight currently open buffer in NERDTree 
    augroup nerdtree_highlight
      autocmd!
      autocmd bufenter * call SyncTree()
    augroup end
"-----------------------------------------------------------------------------------END-OF-NERDTREE-SETTINGS------------------------------------------------------------------------------------------------"  
    
    augroup blamer_toggle_group
      autocmd!
      autocmd bufenter * call BlamerToggle()
    augroup end

    let g:blamer_delay = 300
    highlight Blamer guifg=grey
"------------------------------------------------------------------------------------MULTI-CURSORS-SETTINGS-------------------------------------------------------------------------------------------------"
  "Custom Mappings    
    let g:multi_cursor_use_default_mapping=0
    let g:multi_cursor_start_word_key      = '<C-d>'
    let g:multi_cursor_select_all_word_key = '<A-n>'
    let g:multi_cursor_start_key           = 'g<C-n>'
    let g:multi_cursor_select_all_key      = 'g<A-n>'
    let g:multi_cursor_next_key            = '<C-d>'
    let g:multi_cursor_prev_key            = '<C-p>'
    let g:multi_cursor_skip_key            = '<C-x>'
    let g:multi_cursor_quit_key            = '<Esc>'

"----------------------------------------------------------------------------END-OF-MULTI-CURSORS-SETTINGS-------------------------------------------------------------------------------------------------"

"-----------------------------------------------------------------------------------GITGUTTER-SETTINGS----------------------------------------------------------------------------------------------------"
    "Make Git Gutter Column permanent
      set signcolumn=yes
    
    function IsFloatWindowOpen()
      "Checks if the Float Window is Open
      for nr in range(1, winnr('$'))
        if getwinvar(nr, 'float')
           return win_getid(nr)
        endif  
      endfor
    return 0
    endfunction

    function CloseFloatWinBefore() 
      let isOpen = IsFloatWindowOpen()
      if gitgutter#hunk#in_hunk(line(".")) && isOpen == 0
        GitGutterPreviewHunk
      else
       call coc#util#float_hide() "this functions is hiding the coc float window (ex: eslint and function definition)
       GitGutterPreviewHunk
       "call coc#util#close_win(isOpen)
       "calVl nvim_win_close(isOpen, 1)
      endif
    endfunction
  "--------------------------------------------------------------------------------END-OF-GITGUTTER-SETTINGS----------------------------------------------------------------------------------------------"
  "----------------------------------------------------------------------------------FUZZY-FINDER-SETTINGS------------------------------------------------------------------------------------------------"
    "Removes statusline from search
      autocmd! FileType fzf set laststatus=0 noshowmode noruler
      \| autocmd BufLeave <buffer> set laststatus=2 showmode ruler
    "FZF custom actions 
      let g:fzf_action = {
            \ 'ctrl-h': 'split',
            \ 'ctrl-v': 'vsplit',
            \ 'ctrl-/': 'tab split'}
    "opens files from FZF on split / vsplit and tab mode
      function! s:file_spiler(lines)
        if len(a:lines) < 2 | return | endif
        echo a:lines[0]
        let l:cmd =  get(g:fzf_action, a:lines[0], 'e')
        for l:item in a:lines[1:]
          let l:pos = strridx(l:item, ' ')
          let l:file_path = l:item[pos+1:-1]
        execute 'silent '. l:cmd . ' ' . l:file_path
        endfor
      endfunction

      autocmd VimEnter * command! -bang -nargs=? GFilesWithDevicons call fzf#devicon#vim#gitfiles(<q-args>,{'options': '--no-preview','sink*':function('s:file_spiler'), 'window': 'call FloatingWindowFzf()' },<bang>0)

      "Prevent fzf to open files inside nerdtree
        nnoremap <silent> <expr> <C-p> (expand('%') =~ 'NERD_tree' ? "\<c-w>\<c-w>" : '').":GFilesWithDevicons\<cr>" 
        nnoremap <silent> <expr> <C-f> (expand('%') =~ 'NERD_tree' ? "\<c-w>\<c-w>" : '').":Ag\<cr>"


      "Float Window for FZF
        function! FloatingWindowFzf()
          let height = float2nr(10)
          let width = float2nr(65)
          let top = 0
          let left = (&columns - width) / 2
          let opts = {'relative': 'editor', 'row': top, 'col': left, 'width': width, 'height': height, 'style': 'minimal'}
          let top = "╭" . repeat("─", width - 2) . "╮"
          let mid = "│" . repeat(" ", width - 2) . "│"
          let bot = "╰" . repeat("─", width - 2) . "╯"
          let lines = [top] + repeat([mid], height - 2) + [bot]
          let s:buf = nvim_create_buf(v:false, v:true)
          call nvim_buf_set_lines(s:buf, 0, -1, v:true, lines)
          call nvim_open_win(s:buf, v:true, opts)
          set winhl=Normal:Floating
          let opts.row += 1
          let opts.height -= 2
          let opts.col += 2
          let opts.width -= 4
          call nvim_open_win(nvim_create_buf(v:false, v:true), v:true, opts)
          au BufWipeout <buffer> exe 'bw '.s:buf
        endfunction
  "------------------------------------------------------------------------------END-OF-FUZZY-FINDER-SETTINGS----------------------------------------------------------------------------------------------"
  
  "------------------------------------------------------------------------------------MARKDOWN-SETTINGS---------------------------------------------------------------------------------------------------"
  let g:vim_markdown_folding_disabled = 1
  "--------------------------------------------------------------------------------END-OF-MARKDOWN-SETTINGS------------------------------------------------------------------------------------------------"
  
  "------------------------------------------------------------------------------------GRAMAROUS-SETTINGS---------------------------------------------------------------------------------------------------"
  let g:grammarous#use_vim_spelllang = 1
  "--------------------------------------------------------------------------------END-OF-GRAMAROUS-SETTINGS------------------------------------------------------------------------------------------------"
  
  "--------------------------------------------------------------------------------------COC-SETTINGS------------------------------------------------------------------------------------------------------"
    let g:coc_global_extensions = [
                \ 'coc-snippets',
                   \ 'coc-pairs',
                \ 'coc-tsserver',
                  \ 'coc-eslint', 
                \ 'coc-prettier', 
                    \ 'coc-json', 
                \ ]

  "vim-prettier
    let g:prettier#quickfix_enabled = 0
    let g:prettier#quickfix_auto_focus = 0
    "prettier command for coc
      command! -nargs=0 Prettier :CocCommand prettier.formatFile
      "run prettier on save
        let g:prettier#autoformat = 0
        autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.vue,*.yaml,*.html PrettierAsync
        let g:prettier#config#config_precedence = 'prefer-file'
        let g:prettier#config#parser = 'flow'

  "COC Setup
  
  "if hidden is not set, TextEdit might fail.
  "set hidden  Some servers have issues with backup files, see #649 set nobackup set nowritebackup " Better display for messages set cmdheight=2 "

  "don't give |ins-completion-menu| messages.
    set shortmess+=c


  "Use tab for trigger completion with characters ahead and navigate.
  "Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
    inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
    inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

    function! s:check_back_space() abort
      let col = col('.') - 1
      return !col || getline('.')[col - 1]  =~# '\s'
    endfunction

  "Use <c-space> to trigger completion.
    inoremap <silent><expr> <c-space> coc#refresh()

  "Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
  "Coc only does snippet and additional edit on confirm.
    inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
  "Or use `complete_info` if your vim support it, like:
    inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

  "Use `[g` and `]g` to navigate diagnostics
    nmap <silent> [g <Plug>(coc-diagnostic-prev)
    nmap <silent> ]g <Plug>(coc-diagnostic-next)

 "Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

 "Use K to show documentation in preview window
nnoremap <silent> D :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

 "Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

 "Remap for rename current word
nmap <F2> <Plug>(coc-rename)

 "Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
   "Setup formatexpr specified file type(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
   "Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

 "Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

 "Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
 "Fix auto fix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

 "Create mappings for function text object, requires document symbols feature of languageserver.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

 "Use <C-d> for select selections ranges, needs server support, like: coc-tsserver, coc-python
"nmap <silent> <C-d> <Plug>(coc-range-select)
"xmap <silent> <C-d> <Plug>(coc-range-select)

 "Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

 "Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

 "use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

 "Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

 "Using CocList
 "Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
 "Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
 "Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
 "Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
 "Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
 "Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
 "Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
 "Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
"-------------------------------------------------------------------------------------END-OF-COC-SETTINGS---------------------------------------------------------------------------------------------------"

