##!/bin/sh

echo "---- nvim auto installer ----"

if dpkg -l | grep -o -q neovim
then 
  echo "----- neovim found -----"
  sudo apt-get -qq --only-upgrade install neovim
  echo "---- neovim updated ----"
else
  echo "--- installing neovim ---"
  sudo apt -qq install neovim
  echo "---- updating neovim ----"
  sudo apt-get -qq --only-upgrade install neovim
fi

if [ ! -d "~/.config/nvim" ]
then
  echo "nvim dir exists"
else
mkdir ~/.config/nvim
fi
