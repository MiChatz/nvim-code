## TODO
 - Check jsx comments are not working
 - Check javascript multi line comments
 - NerdTree Git needs a key stroke to add colors 
 - After a git change the / are back on NerdTree
 - Make the open file highlight on the NerdTree more visible
 - Dev Icons color on different directories are not the same!
 - Remove arrows on side bar from eslint(leave number colors). It overrides the gitgutter lines
 - Some filenames after extension have an * (NerdTree?)
 - GitGutter pop Up has white and blue insted of green and red! why?
 - Change Bg color and border of fzf floating window 
 - Check the prettier setup according to thera
 - Delete is acting like cut why? should be fixed
 - Fix the appearance of fzf ag (universal search) check: [fzf-preview.vim](https://github.com/yuki-ycino/fzf-preview.vim)

## Installation
 1. [neovim](https://github.com/neovim/neovim)
 1. [vim-plug](https://github.com/junegunn/vim-plug)
 1. [fzf](https://github.com/junegunn/fzf)
 1. [cargo](https://doc.rust-lang.org/cargo/commands/cargo-install.html)
 1. [devicon-lookup](https://github.com/coreyja/devicon-lookup)
 1. [fzf-devicon](https://github.com/coreyja/fzf.devicon.vim)
 1. [instant-markdown](https://github.com/suan/vim-instant-markdown)
 1. [silversearcher-ag](https://github.com/ggreer/the_silver_searcher)

 
## coc And Solargraph
#### I have an issue with coc and solargraph-39.6
 - gem install solargraph -v 0.39.3
 - :CocInstall coc-solargraph

### Fuzzy Finder
 find by filenames  
 press `Ctrl + p`
 find in files  
 press `Ctrl + f`

### NerdTree keys
- Toggle
  press `Ctrl + b`
- Got to Parent folder
  press `p`
- Go to Root folder
  press `P`
- Close Parent folder recursive
  press `x`
- Close Root folder recursive
  press `P + X`
- Open file horizontally
  press s
- Open file vertically
  press i
- Add new file / directory
  go to parent folder and press `m` to open the NerdTree Menu
- Show git ignore file  
  Shift + i
- Open file / horizontal / vertical  
  press o / h / v

### Use the GitGutter (tool that shows the git diffs):
  - hover the line with cursor
  - press `gp` to toggle the preview window
  - press `gs` to accept the change
  - press `gu` to undo the change

### Open Project:
  - `$ nvim <nameOfTheDirectory>`
  - enter the directory and exec `$ nvim`

### Multi line comments 
  select text and press `Ctrl + /`

### Jump between open windows inside Vim
  press `Ctrl + arrows`

### Page Top
  `Shift + Up` arrow

### Page Bottom
  `Shift + Down` arrow

### Insert Mode:
  press `i`

### Exit Insert Mode:
  press `Esc`

### Execute command
  press `: <theCommand> Enter`

### Select text by line:
  press `Shift + V + (navigate)`

### Select text by letter:
  press `v + (navigate)`

### Exit vim:
  execute `:q`

### Exit Vim anyway
  execute `:q!`

### Save:
    - 1)
    execute `:w`
    - 2)
    press `Ctrl + s`

### Save by override
  execute `:w!`

### Copy:
  after selecting the text press `y`

### Cut:
  after selecting the text press `d`

### Paste:
  after selecting the text press `p`

### Tab Indentation:
 - on normal mode 
  - for a single line type `Shift + >>` / `Shift + <<` the `.` will repeat the command.
  - for multi line select the block and type `Shift + >` / `Shift + <` the `.` will repeat the command.
### Search:
    - 1)
    `:/<searchWord>`
    next result press `n`
    prev result press `N`
    - 2)
    `:?<searchWord>`
    next result pres `n`
    prev result press `N`
    - Exit Search
    :noh
### Tmux keys 
[cheatsheet](https://gist.github.com/henrik/1967800)

#### Panes (splits)

- %  horizontal split
- "  vertical split
- o  swap panes
- q  show pane numbers
- x  kill pane
- ⍽  space - toggle between layouts
